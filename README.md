# QualysGuard APIV2 - Custom Asset Report

With this little tool, we can download simple asset report from QualysGuard
using the APIV2 (more secure) connection and transform the output format to
generate a compatible xml file with Microsoft Excel. 

*The specific format uses the name group assets instead of the id group assets

## Running the program

* Clone the repo...

Command line

    :::bash
	        git clone git@bitbucket.org:mateito505/qualysguard-apiv2.git


* Rename the file "skeleton" to ".qcrc"
* Setup this configurarion file with your information (".qcrc")
* Give permissions to execute and run it...

Command line

    :::bash
	        chmod +x qualysapi-bc.py
			python qualysapi-bc.py

* Now you can open the result file "Asset Report.xml" with Microsoft Excel.
